package rs.poljohit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by mawl on 1/20/17.
 */
@SpringBootApplication
public class Bookee {
    public static void main (String[] args) {
        SpringApplication.run(Bookee.class, args);
    }
}
