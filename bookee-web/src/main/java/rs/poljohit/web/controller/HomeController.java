package rs.poljohit.web.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mawl on 1/22/17.
 */
@RestController
@RequestMapping
@Log4j2
public class HomeController {
    @GetMapping("/")
    public String home () {
        System.out.println("test");
        return "test";
    }
}
